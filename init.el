;;; Set up package manager
(defvar bootstrap-version)

(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-pull-recipe-repositories)
(straight-use-package 'use-package)
(use-package straight
  :custom (straight-use-package-by-default t))

;;; Theme, modeline
(use-package modus-themes
  :init
  (setq modus-themes-italic-constructs t
        modus-themes-bold-constructs nil)
  :config
  (modus-themes-select 'modus-operandi)
  (setq modus-themes-to-toggle '(modus-operandi modus-vivendi-tinted))

  :bind
  ("<f5>" . modus-themes-toggle))

;; no splash
(setq inhibit-splash-screen t)

(setq initial-major-mode 'fundamental-mode)  ; default mode for the *scratch* buffer
(setq display-time-default-load-average nil) ; this information is useless for most

;; Automatically reread from disk if the underlying file changes
;; (setq auto-revert-interval 1)
;; (setq auto-revert-check-vc-info t)
;; (global-auto-revert-mode)

;; Save history of minibuffer
(savehist-mode)

;; Move through windows with Ctrl-<arrow keys>
(windmove-default-keybindings 'control) ; You can use other modifiers here

;; Adjust font size. Use "C-x C-0" to restore size and then tweak
(global-set-key  "\M-+" 'text-scale-increase)
(global-set-key  "\M-_" 'text-scale-decrease)

;; Fix archaic defaults
(setq sentence-end-double-space nil)

;; Make right-click do something sensible
(when (display-graphic-p)
  (context-menu-mode))

;; Don't litter file system with *~ backup files; put them all inside
;; ~/.emacs.d/backup or wherever
(defun bedrock--backup-file-name (fpath)
  "Return a new file path of a given file path.
If the new path's directories does not exist, create them."
  (let* ((backupRootDir "~/.emacs-backup/")
         (filePath (replace-regexp-in-string "[A-Za-z]:" "" fpath )) ; remove Windows driver letter in path
         (backupFilePath (replace-regexp-in-string "//" "/" (concat backupRootDir filePath "~") )))
    (make-directory (file-name-directory backupFilePath) (file-name-directory backupFilePath))
    backupFilePath))
(setq make-backup-file-name-function 'bedrock--backup-file-name)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Discovery aids
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Show the help buffer after startup
;; (add-hook 'after-init-hook 'help-quick)

;; which-key: shows a popup of available keybindings when typing a long key
;; sequence (e.g. C-x ...)
(use-package which-key
  :config
  (which-key-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Minibuffer/completion settings
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; For help, see: https://www.masteringemacs.org/article/understanding-minibuffer-completion

(setq enable-recursive-minibuffers t)                ; Use the minibuffer whilst in the minibuffer
(setq completion-cycle-threshold 1)                  ; TAB cycles candidates
(setq completions-detailed t)                        ; Show annotations
(setq tab-always-indent 'complete)                   ; When I hit TAB, try to complete, otherwise, indent
(setq completion-styles '(basic initials substring)) ; Different styles to match input to candidates

(setq completion-auto-help 'always)                  ; Open completion always; `lazy' another option
(setq completions-max-height 20)                     ; This is arbitrary
(setq completions-detailed t)
(setq completions-format 'one-column)
(setq completions-group t)
(setq completion-auto-select 'second-tab)            ; Much more eager
                                        ;(setq completion-auto-select t)                     ; See `C-h v completion-auto-select' for more possible values

(keymap-set minibuffer-mode-map "TAB" 'minibuffer-complete) ; TAB acts more like how it does in the shell

;; For a fancier built-in completion option, try ido-mode or fido-mode. See also
;; the file extras/base.el
;(fido-vertical-mode)
;(setq icomplete-delay-completions-threshold 4000)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Interface enhancements/defaults
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Mode line information
(setq line-number-mode t)                        ; Show current line in modeline
(setq column-number-mode t)                      ; Show column as well

(setq x-underline-at-descent-line nil)           ; Prettier underlines
(setq switch-to-buffer-obey-display-actions t)   ; Make switching buffers more consistent

(setq-default show-trailing-whitespace nil)      ; By default, don't underline trailing spaces
(setq-default indicate-buffer-boundaries 'left)  ; Show buffer top and bottom in the margin

;; Sometimes, 'meta' is mapped to the wrong modifier key. Not
;; exactly sure why, but differs depending on brew vs EmacsForOSX,
;; for example.
(setq mac-command-modifier 'meta)
(setq mac-option-modifier 'super)

;; Make M-h do the macos thing ...
(global-set-key  (kbd "M-h") 'ns-do-hide-emacs)


;; Enable horizontal scrolling
(setq mouse-wheel-tilt-scroll t)
(setq mouse-wheel-flip-direction t)

(setq-default indent-tabs-mode nil)
(setq-default tab-width 2)

;; Misc. UI tweaks
(blink-cursor-mode -1)                                ; Steady cursor
(pixel-scroll-precision-mode)                         ; Smooth scrolling

;; Display line numbers in programming mode
(add-hook 'prog-mode-hook 'display-line-numbers-mode)
(setq-default display-line-numbers-width 3)           ; Set a minimum width

;; Nice line wrapping when working with text
(add-hook 'text-mode-hook 'visual-line-mode)

;; Modes to highlight the current line with
;; (let ((hl-line-hooks '(text-mode-hook prog-mode-hook)))
;;   (mapc (lambda (hook) (add-hook hook 'hl-line-mode)) hl-line-hooks))

(global-set-key [f6] 'set-selective-display)

(defun timestamp ()
  "Insert current time and date at point."
  (interactive)
  (insert (current-time-string)))
(global-set-key [f7] 'timestamp)

(transient-mark-mode 0)
(setq scroll-step 2)

(global-prettify-symbols-mode +1)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Tab-bar configuration
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Show the tab-bar as soon as tab-bar functions are invoked
(setq tab-bar-show 0)

;; Add the time to the tab-bar, if visible
(add-to-list 'tab-bar-format 'tab-bar-format-align-right 'append)
(add-to-list 'tab-bar-format 'tab-bar-format-global 'append)
(setq display-time-format "%a %F %T")
(setq display-time-interval 1)
(display-time-mode)

(use-package eglot
  :config
  (add-to-list 'eglot-server-programs
               '((elixir-mode heex-mode) .
                 ("~/hacking/lexical/_build/dev/package/lexical/bin/start_lexical.sh"))))


(use-package treemacs
  :hook
  (treemacs-mode-hook . (lambda () (display-line-numbers-mode -1)))
  :config
  (treemacs-filewatch-mode t)
  (treemacs-git-mode 'simple))

;; Elixir fun (see what I did there?)
;; 
(use-package elixir-mode
  :hook
  (elixir-mode . eglot-ensure)
  (elixir-mode . (lambda ()
                    (push '(">=" . ?\u2265) prettify-symbols-alist)
                    (push '("<=" . ?\u2264) prettify-symbols-alist)
                    (push '("!=" . ?\u2260) prettify-symbols-alist)
                    (push '("==" . ?\u2A75) prettify-symbols-alist)
                    (push '("=~" . ?\u2245) prettify-symbols-alist)
                    (push '("<-" . ?\u2190) prettify-symbols-alist)
                    (push '("->" . ?\u2192) prettify-symbols-alist)
                    (push '("<-" . ?\u2190) prettify-symbols-alist)
                    (push '("|>" . ?\u25B7) prettify-symbols-alist))))

;; Helper functions to discover path for Erlang elixir tools
;;
;; We'll assume asdf installation
;; erlang-asdf-current-tools:
;;   - asdf which erl --> <path-to-asdf>/install/erlang/<some-version>/bin/erl
;;     asdf-current-tools --> <....>/bin/../lib/tools-<version>/
;;
(setq erlang-asdf-current-lib
      (string-join
       (append 
        (butlast
         (file-name-split(file-name-directory (shell-command-to-string "asdf which erl"))) 2)
        '("lib/"))
       "/"))
(setq erlang-asdf-current-tools
      (concat
       erlang-asdf-current-lib
       (file-name-completion "tools" erlang-asdf-current-lib)
       "emacs"))

(use-package erlang
  :load-path (erlang-asdf-current-tools)
  :config (add-to-list 'eglot-server-programs
         '(erlang-mode "~/bin/erlang_ls"))
  :hook (erlang-mode . eglot-ensure)
  :mode (("\\.erl?$" . erlang-mode)
         ("rebar\\.config$" . erlang-mode)
         ("relx\\.config$" . erlang-mode)
         ("sys\\.config\\.src$" . erlang-mode)
         ("sys\\.config$" . erlang-mode)
         ("\\.config\\.src?$" . erlang-mode)
         ("\\.config\\.script?$" . erlang-mode)
         ("\\.hrl?$" . erlang-mode)
         ("\\.app?$" . erlang-mode)
         ("\\.app.src?$" . erlang-mode)
         ("\\Emakefile" . erlang-mode)))

(setq flymake-allowed-file-name-masks nil) ;; hack until patch for obsolete flymake dep is live
(use-package haskell-mode
  :hook (haskell-literate-mode . eglot-ensure)
  :hook (haskell-mode . eglot-ensure)
  :config (setq-default eglot-workspace-configuration
                        '((haskell
                           (plugin
                            (stan
                             (globalOn . :json-false))))))  ;; disable stan
  :custom
  (eglot-autoshutdown t)  ;; shutdown language server after closing last file
  (eglot-confirm-server-initiated-edits nil)  ;; allow edits without confirmation
  )

(use-package yasnippet :diminish yas-minor-mode :config (yas-global-mode 1))
(use-package yasnippet-snippets :after (yasnippet))

(setq parens-require-spaces nil)
;;
;; single-quotes
(defun insert-quotations (&optional arg)
  "Enclose following ARG sexps in quotation marks.
Leave point after open-paren."
  (interactive "*P")
  (insert-pair arg ?\' ?\'))
(global-set-key "\M-'" 'insert-quotations)

;;
;; double-quotes
(defun insert-quotes (&optional arg)
  "Enclose following ARG sexps in quotes.
Leave point after open-quote."
  (interactive "*P")
  (insert-pair arg ?\" ?\"))
(global-set-key "\M-\"" 'insert-quotes)

;;
;; backquotes
(defun insert-double-backquote (&optional arg)
  "Enclose following ARG sexps in quotations with double backquotes.
Leave point after open-quotation."
  (interactive "*P")
  (insert-pair arg ?\` ?\`))
(global-set-key (kbd "C-M-'") 'insert-double-backquote)

;;
;; braces
(defun insert-braces (&optional arg)
  "Enclose following ARG sexps in curly braces.
Leave point after open-brace."
  (interactive "*P")
  (insert-pair arg ?\{ ?\}))
(define-key global-map (kbd "M-{") nil)
(global-set-key (kbd "M-{") 'insert-braces)

;;
;; surrounding spaces
(defun insert-surrounding-spaces (&optional arg)
  "wrap following ARG sexps with a space.
Leave point after first space."
  (interactive "*P")
  (insert-pair arg ?\ ?\ ))
(define-key global-map (kbd "S-M-SPC") nil)
(global-set-key (kbd "S-M-SPC") 'insert-surrounding-spaces)

;;
;; square brackets
(defun insert-brackets (&optional arg)
  "Enclose following ARG sexps in square brackets.
Leave point after open-bracket."
  (interactive "*P")
  (insert-pair arg ?\[ ?\]))
(global-set-key (kbd "M-[") 'insert-brackets)
(global-set-key (kbd "M-]") 'up-list)

;;
;; Elixir pipe
(defun insert-elixir-pipe ()
  "Insert |> sequence followed by a space."
  (interactive nil)
  (insert "|> ")
  (indent-relative-first-indent-point))
(global-set-key (kbd "C-|") 'insert-elixir-pipe)
(global-set-key (kbd "C->") 'insert-elixir-pipe)
(global-set-key (kbd "C-«") 'insert-elixir-pipe)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Built-in customization framework
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages '(which-key)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
